<?php

namespace Drupal\masquerade_extra\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Configure settings for Masquerade.
 */
class MasqueradeSettingsForm extends ConfigFormBase {
  
  use StringTranslationTrait;
  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'masquerade_admin_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'masquerade_extra.settings',
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('masquerade_extra.settings');    
    $form['masquerade']['options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Default options'),
      '#default_value' => $config->get('options'),
      '#options' => [
        'uid' => $this->t('Serach with Users ID'),
        'mail' => $this->t('Serach with Users Email'),
        'roles' => $this->t('Serach with Users Role'),
      ],
      '#description' => $this->t('Users with the <em>Masquerade Permissions</em> '
        . 'will be able to search with abpve provided options.'),
    ]; 

    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      // Retrieve the configuration
       $this->configFactory->getEditable('masquerade_extra.settings')
      // Set the submitted configuration setting
      ->set('options', $form_state->getValue('options'))
      ->save();
      
    parent::submitForm($form, $form_state);
  }
}

