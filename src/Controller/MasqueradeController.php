<?php

namespace Drupal\masquerade_extra\Controller;

use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\masquerade_extra\MasqueradeMatcher;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MasqueradeController extends \Drupal\system\Controller\EntityAutocompleteController {

  /**
   * The auto-complete matcher for entity references.
   */
  protected $matcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(MasqueradeMatcher $matcher, KeyValueStoreInterface $key_value) {
    $this->matcher = $matcher;
    $this->keyValue = $key_value;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('masquerade_extra.autocomplete_matcher'), $container->get('keyvalue')->get('entity_autocomplete')
    );
  }

}
