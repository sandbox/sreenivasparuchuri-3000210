<?php

namespace Drupal\masquerade_extra\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

class MasqueradeRouteSubscriber extends RouteSubscriberBase {

  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('system.entity_autocomplete')) {
      $route->setDefault('_controller', '\Drupal\masquerade_extra\Controller\MasqueradeController::handleAutocomplete');
    }
  }

}

