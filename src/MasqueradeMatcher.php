<?php

namespace Drupal\masquerade_extra;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\user\Entity\User;

class MasqueradeMatcher extends \Drupal\Core\Entity\EntityAutocompleteMatcher {

  /**
   * Overridden function of getMatches().
   * @see \Drupal\system\Controller\EntityAutocompleteController
   * @param string $target_type
   *   The ID of the target entity type.
   * @param string $selection_handler
   *   The plugin ID of the entity reference selection handler.
   * @param array $selection_settings
   *   An array of settings that will be passed to the selection handler.
   * @param string $string
   *   (optional) The label of the entity to query by.
   *
   * @return array
   *   An array of matched entity labels, in the format required by the AJAX
   *   autocomplete API (e.g. array('value' => $value, 'label' => $label)).
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown when the current user doesn't have access to the specified entity.   *
   * 
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = '') {
    $matches = [];
    if (isset($selection_settings['filter']) && isset($target_type) && $target_type == 'user' && $selection_settings['masquerade_multiple']) {
      $match_operator = !empty($selection_settings['match_operator']) ? $selection_settings['match_operator'] : 'CONTAINS';
      $entity_data = $this->getEntities($match_operator, $string, 10);
      $matches = $entity_data;
    }
    else {
      $matches = parent::getMatches($target_type, $selection_handler, $selection_settings, $string);
    }
    return $matches;
  }

  /**
   * {@inheritdoc}
   * Gets matched labels based on a given search string.
   */
  protected function getEntities($match_operator, $string, $records) {
    $matches = [];
    $query = \Drupal::entityQuery('user');
    $masquerade_config = \Drupal::config('masquerade_extra.settings');
    $conditions = $query->orConditionGroup();
    $conditions->condition('name', $string, $match_operator);
    foreach($masquerade_config->get('options') as $key => $value) {
      if(!empty($value)) {
        $conditions->condition($key, $string, $match_operator);
      }      
    }    
    $query
      ->condition('status', 1)
      ->condition($conditions)
      ->range(0, $records);
    $entity_ids = $query->execute();
    foreach ($entity_ids as $key => $value) {
      $users = User::load($value);
      $key = $users->getAccountName() . " ($key)";
      // Strip things like starting/trailing white spaces, line breaks and
      // tags.
      // Names containing commas or quotes must be wrapped in quotes. 
      $key = Tags::encode(preg_replace('/\s\s+/', ' ', str_replace("\n", '', 
        trim(Html::decodeEntities(strip_tags($key))))));
      $matches[] = ['value' => $key, 'label' => $users->getAccountName()];
    }
    return $matches;
  }

}
