This module extend the default Masquerade Module autocomplete behaviour so that 
we can search with both user name, user id, user email and  user role.

Configure link admin/config/masquerade/settings was provided where we can select
options to search in Masquerade Autocomplete functionality
